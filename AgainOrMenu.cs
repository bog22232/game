﻿using System;

namespace Game
{
    class AgainOrMenu
    {
        private ConsoleKeyInfo _cki;
        private string _againOrMenu;
        private string _again = "Again";
        private string _menu = "Menu";
        private string _pick = "*****";
        private string _space = "        ";
        public string AgOrMen()
        {
            _againOrMenu = _again;
            Console.Clear(); 
            Console.WriteLine($"{_again} or {_menu}?");
            Console.WriteLine($"{_pick} {_space}");
            while (true)
            {
                _cki = Console.ReadKey();
                if (_cki.Key == ConsoleKey.RightArrow)
                {
                    Console.Clear();
                    _againOrMenu = _menu;
                    Console.WriteLine($"{_again} or {_menu}?");
                    Console.WriteLine($"{_space} {_pick}");
                }
                else if (_cki.Key == ConsoleKey.LeftArrow)
                {
                    Console.Clear();
                    _againOrMenu = _again;
                    Console.WriteLine($"{_again} or {_menu}?");
                    Console.WriteLine($"{_pick} {_space}");
                }
                else if(_cki.Key == ConsoleKey.Enter)
                {
                    if (_againOrMenu == _again)
                    {
                        return _again;
                    }
                    else if (_againOrMenu == _menu)
                    {
                        return _menu;
                    }
                }             
            }
        }
    }
}
