﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class LevelsMenu
    {
        private int _numCoins;
        private string _lvl1Str = "Level 1";
        private string _lvl2Str = "Level 2";
        private string _lvl3Str = "Level 3";
        private string _pick = "<-";
        private ConsoleKeyInfo _cki;

        List<string[]> levelsList = new List<string[]>();


       public LevelsMenu()
        {
            levelsList.Add(new string[] { "┌────────────────┐" });
            levelsList.Add(new string[] { "│", $"{_lvl1Str}", $"{_pick}" });
            levelsList.Add(new string[] { "│", $"{_lvl2Str}", "" });
            levelsList.Add(new string[] { "│", $"{_lvl3Str}", "" });
            levelsList.Add(new string[] { "└────────────────┘" });
        }
        private void FieldOnDisplay()
        {
            Console.Clear();
            foreach (var t in levelsList)
            {
                Console.WriteLine(string.Join("", t));
            }
        }


        public void PickLevel()
        {
            int x = 2;
            int y = 1;
            while (true)
            {
                FieldOnDisplay();
                _cki = Console.ReadKey();

                if (_cki.Key == ConsoleKey.UpArrow)
                {
                    if (levelsList[y - 1][x] != "─")
                    {
                        levelsList[y - 1][x] = _pick;
                        levelsList[y][x] = "";
                        y--;
                    }
                }
                else if (_cki.Key == ConsoleKey.DownArrow)
                {
                    if (levelsList[y + 1][x] != "─")
                    {
                        levelsList[y + 1][x] = _pick;
                        levelsList[y][x] = "";
                        y++;
                    }
                }
                else if (_cki.Key == ConsoleKey.Enter)
                {
                    if (levelsList[y][x - 1] == _lvl1Str)
                    {
                        _numCoins = 5;
                    }

                    if (levelsList[y][x - 1] == _lvl2Str)
                    {
                        _numCoins = 8;
                    }

                    if (levelsList[y][x - 1] == _lvl3Str)
                    {
                        _numCoins = 12;
                    }
                    break;
                }
            }
        }

        public int GetLevel()
        {
            return _numCoins;
        }
    }
}
