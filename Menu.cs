﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class Menu
    {
        private List<string[]> menuList = new List<string[]>();
        private ConsoleKeyInfo _cki;
        private readonly string _newGame = "New game";
        private string _lvlGame = "Levels";
        private readonly string _exit = "Exit";
        private string _pick= "<-";
        private static int k = 1;
        private string _pickOption;
        public Menu()
        {
            menuList.Add(new string[] { "┌────────────────┐"});
            menuList.Add(new string[] { "│", $"{_newGame}",$"{_pick}"  });
            menuList.Add(new string[] { "│", $"{_lvlGame}", "" });
            menuList.Add(new string[] { "│", $"{_exit}", "" });
            menuList.Add(new string[] { "└────────────────┘" });
 
        }

        private void FieldOnDisplay()
        {
            Console.Clear();
            foreach (var t in menuList)
            {
                Console.WriteLine(string.Join("", t));
            }
        }

        public string PickOption()
        {
            while (true)
            {
                FieldOnDisplay();

                _cki = Console.ReadKey();
                if (_cki.Key == ConsoleKey.UpArrow)
                {
                    try
                    {
                        if (menuList[k - 1][2] != "─")
                        {
                            k--;
                            menuList[k][2] = _pick;
                            menuList[k + 1][2] = "";
                        }
                    }
                    catch
                    {
                    }
                }

                else if (_cki.Key == ConsoleKey.DownArrow)
                {
                    try
                    {
                        if (menuList[k + 1][2] != "─")
                        {
                            k++;
                            menuList[k][2] = _pick;
                            menuList[k - 1][2] = "";
                        }
                    }
                    catch
                    {
                    }
                }
                else if (_cki.Key == ConsoleKey.Enter)
                {
                    return menuList[k][1]; 
                    break;
                }
            }
        }
    }   
}
