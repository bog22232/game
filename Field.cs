﻿using System;
using System.Collections.Generic;

namespace Game
{
    class Field
    {
        private List<string[]> frame = new List<string[]>();
        private string _wall = "*";
        private string _fon = " ";
        private string _hero = "X";
        private string _coin = "@";
        private string _grass = "#";
        private int _numCoin;
        private static int _numCoinNeed;
        private Gamer gamer;
        private ConsoleKeyInfo _cki;
        public Field()
        {
            frame.Add(new string[] { "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "X", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", "*" });
            frame.Add(new string[] { "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*", "*" });
        }
        public void GenCoins(int num)
        {
            _numCoinNeed = num;
            Random rand = new Random();
            for (int i =0; i < num; i++)
            {                
                int randX = rand.Next(1, frame.Count - 1);
                int randY = rand.Next(1, frame[randX].Length - 1);
                frame[randX][randY] = _coin;
            }
        }

        private void YouWin(int time)
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("* * * * * * * * * * * * * * * * *");
            Console.WriteLine("***!!!!YOU WIN!!!!****");
            Console.WriteLine("* * * * * * * * * * * * * * * * *");
            Console.WriteLine($"****Ваше время раунда : {time}c.****");
        }
        private void FieldOnDisplay()
        {
            Console.Clear();
            Console.WriteLine($"Очки: {_numCoin} @");
            for (int x = 0; x < frame.Count; x++)
            {                
                Console.WriteLine(string.Join("", frame[x]));                
            }
        }

        public void Move(Gamer gamer)
        {
            var _startTime = DateTime.Now;

            Console.Clear();
            Console.WriteLine("Введите имя игрока:");
            gamer.Name = Console.ReadLine();

            while (true)
            {
                if (_numCoin == _numCoinNeed)
                {
                    int _gameTime = Convert.ToInt32((DateTime.Now - _startTime).TotalSeconds);
                    YouWin(_gameTime);                     
                    Console.ReadKey();
                    gamer.Time = _gameTime;
                    break;
                }
                for (int x = frame.Count - 1; x >= 0; x--)
                {
                    for (int y = 0; y < frame[x].Length; y++)
                    {
                        if (frame[x][y] == _hero)
                        {
                            FieldOnDisplay();

                            _cki = Console.ReadKey();

                            if (_cki.Key == ConsoleKey.UpArrow)
                            {
                                if (x - 1 >= 0 && frame[x - 1][y] == _fon)
                                {
                                    frame[x][y] = _fon;
                                    frame[x - 1][y] = _hero;
                                }
                                else if (x - 1 >= 0 && frame[x - 1][y] == _coin)
                                {
                                    frame[x][y] = _fon;
                                    frame[x - 1][y] = _hero;
                                    _numCoin++;
                                }
                            }
                            else if (_cki.Key == ConsoleKey.DownArrow)
                            {
                                if (x + 1 <= frame.Count - 1 && frame[x + 1][y] == _fon)
                                {
                                    frame[x][y] = _fon;
                                    frame[x + 1][y] = _hero;
                                }
                                else if (x + 1 <= frame.Count - 1 && frame[x + 1][y] == _coin)
                                {
                                    frame[x][y] = _fon;
                                    frame[x + 1][y] = _hero;
                                    _numCoin++;
                                }
                            }
                            else if (_cki.Key == ConsoleKey.LeftArrow)
                            {
                                if (y - 1 >= 0 && frame[x][y - 1] == _fon)
                                {
                                    frame[x][y] = _fon;
                                    frame[x][y - 1] = _hero;
                                }
                                else if (y - 1 >= 0 && frame[x][y - 1] == _coin)
                                {
                                    frame[x][y] = _fon;
                                    frame[x][y - 1] = _hero;
                                    _numCoin++;                                   
                                }
                            }
                            else if (_cki.Key == ConsoleKey.RightArrow)
                            {
                                if (y + 1 <= frame[x - 1].Length && frame[x][y + 1] == _fon)
                                {
                                    frame[x][y] = _fon;
                                    frame[x][y + 1] = _hero;
                                }
                                else if (y + 1 <= frame[x - 1].Length && frame[x][y + 1] == _coin)
                                {
                                    frame[x][y] = _fon;
                                    frame[x][y + 1] = _hero;
                                    _numCoin++;
                                }
                            }                      
                        }
                    }
                }
            }
        }
    }
}